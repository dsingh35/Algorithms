/*
    @filename - testBST.cpp
    @author - Devinder Singh
    @date - 2018/07/01 
*/
#include <cmath>
#include <climits>
#include <ctime>
#include <vector>
#include <algorithm>
#include "Stack.h"
#include <cstdlib>
#include "BST.h"

using namespace std;

/*
 * @param n, the size of vector
 * @return vector, the vector filled with random integers
 * this function generates random numbers from -n to n
*/
vector<int> genData(int n) { 
    vector<int> a(n*2-1);
    for( vector<int>::iterator it = a.begin(); it != a.end(); ++it) {
        int lower = rand() % (n + 1) + (-n);
        int upper = rand() % (n + 1);
        *it = lower + upper;
    }
    return a;
}

/*
 * @return int, the input by user
 * this function takes input from the user 
*/
int getInput() { 
    int n;
    cout << "Enter a positive integer: ";
    cin >> n;
    return n;
}

/*
 * @param a, the vector containing integers
 * this function prints every element of vector
*/
void printList(vector<int> a) { 
    for ( vector<int>::iterator it = a.begin(); it != a.end(); ++it ) 
        cout << *it << " "; 
    cout << endl;
}

/*
 * @param b, the pointer to BSTNode<T> object
 * this method prints tree in inOrder 
*/
template<class T>
void printBT(BSTNode<T>* b) {
    if (b == nullptr) 
        return;
    printBT(b->left);
    cout << b->elm << " ";
    printBT(b->right);
}

/*
 * @param b, the pointer to BSTNode<T> object
 * this method prints tree in inOrder 
*/
template<class T>
void printBT(BST<T> b) {
    printBT(b.root);
    cout << endl;
}

/*
 * @param a, the vector containing data
 * @param start, the start of the vector
 * @param end, the end of the vector
 * this function creates the BST from array
*/
template<class T>
BSTNode<T>* makeBST(vector<T> a, int start, int end) {
    if (start > end ) return nullptr;
    int mid = (start+end)/2;
    BSTNode<T>*element = new BSTNode<T>( a[mid] );
   element->left = makeBST(a, start, mid-1);
   element->right = makeBST(a, mid+1, end);
    return element;
}

/*
 * @param a, the vector containing data
 * this function calls the makeBST function
*/
template<class T>
BST<T> makeBST(vector<T> a) {
    sort(a.begin(), a.end());
    BSTNode<T>*element = makeBST(a,0,a.size()-1);
    BST<T> bst(element);  
    return bst;
}

/*
 * @paramelement, pointer to the BSTNode<T>
 * this function calculates the height of the tree
*/
template<class T>
int height( BSTNode<T> *root) {
    if (root == nullptr ) return 0;
    return max( height(root->left), height(root->right) ) + 1;
}

template<class T>
int height( BST<T> b) {
    height(b.root);    
}

/*
 * @param bracket, the bracket whose opposite we want to find
 * return char, the opposite bracket
*/
char getRequiredBraket(char bracket){
    switch(bracket){
        case '}':
        return '{';
        case ')':
        return '(';
        case ']':
        return '[';
    }
}

/*
 * @param s, the string we want to reverse
*/
void reverseString(string & s){
    string c = s;
    for(int i = c.length()-1,j=0; i >= 0; i--,j++){
        s[j] = c[i];
    }
}

/* @param stack, the object of class Stack
 * @param infix , the string we want to change
 * @counter , to iterate through string
 * this function changes infix to prefix
*/
string Infix_PostfixExpr(string &infix){
    Stack<char> stack;  
    int counter = 0;  
    string result = "";
     infix = '(' + infix + ')'; // adding brackets to escape specail cases
   
        for( counter = 0; counter < infix.length(); counter++ ){
        //if the input is alphabet or digit
        if(infix[counter] != ')' && infix[counter] != ']'&& infix[counter] != '['&& infix[counter] != '}' && infix[counter] != '{' && infix[counter]!= '(' && infix[counter] != '+' && infix[counter] != '-' && infix[counter] != '*' && infix[counter] != '/')
        {
            result+= infix[counter];
        }
        else if(infix[counter] == '(' || infix[counter] == '{' || infix[counter] == '[' )
            stack.push(infix[counter]);
        else if(infix[counter] == ']' || infix[counter] == ')' || infix[counter] == '}'){
            char requiredBRacket = getRequiredBraket(infix[counter]);// getting the opposite bracket
            while(!stack.isEmpty() && stack.getTop() != requiredBRacket){
               if(stack.getTop() == '}' || stack.getTop() == ')' || stack.getTop() == ']' || stack.getTop() == '}' || stack.getTop() == ')' || stack.getTop() == ']')
                    {cout <<  "Error: '" << requiredBRacket << "' was expected at" <<  " '" << stack.getTop() << "'" << endl;
                    return "";
                }
                char pop = stack.getTop();
                result+= pop;
                stack.pop();
            }
            if(stack.getTop() == '(')
                stack.pop();
        }
        else{ // if the input is operator
                while(!stack.isEmpty())
                {   
                    if((infix[counter] == '/' || infix[counter] == '*') &&( stack.getTop() == '+' || stack.getTop() == '-')){
                        char pop = stack.getTop();
                        result+= pop;
                        stack.pop();
                    }
                    else
                        break;                 
                }    
            
            stack.push(infix[counter]); 
        }
    }
  
    while(!stack.isEmpty())
    {
        char c = stack.getTop();
        stack.pop();
        result += c;
    }

    return result; 
}

/*
 * @paramelement, pointer to the BSTNode<T>
 * @return BSTNode<T> , pointer to the node having minimum value
 * this function finds the mininum  node
*/
template<class T>
BSTNode<T>* findMinNode(BSTNode<T>*element)
{
	BSTNode<T>* current =element;
	while(current->left != nullptr)
	{
		current = current->left;
	}

	return current;
}

/*
 * @param elm, the value of the node
 * @paramelement, pointer to the BSTNode<T> that is going to be deleted
*/
template<class T>
BSTNode<T>* remove(T value,BSTNode<T>*element)
{
	if(value <element->elm)
		element->left = remove(value,element->left);
	else if(value > element->elm)
		element->right = remove(value,element->right);
	else
	{
		if(element->left == nullptr && element->right == nullptr)
		{
			delete element;
			return nullptr;
		}
		else if(element->left == nullptr)
		{
			BSTNode<T>* temp = element->right;
			delete element;
			return temp;
		}
		else if(element->right == nullptr)
		{
			BSTNode<T>* temp = element->left;
			delete element;
			return temp;
		}
		else
		{
			BSTNode<T>* tmp = findMinNode(element->right);
			element->elm = tmp->elm;
			element->right = remove(tmp->elm,element->right);
		}
		return element;	 
	}
}	

template<class T>
void clear(BSTNode<T>* root){
    if( root == nullptr ) 
        return;
    clear(root->left);
    clear(root->right); 
    delete root;     
}

template<class T>
void remove(T value,BST<T> tree){
    remove(value,tree.root);
}

/*
 * @param b, the tree from where we want elements
 * @param array, the in which we want to store
*/
void getBTElements(BST<int> b,vector<int>& array) {
   if (b.root == nullptr) return;
    getBTElements(b.root->left,array);
    array.push_back(b.root->elm);
    getBTElements(b.root->right,array);
}

/*
 * @param bst1, the tree we want to merge
 * @param bst2, the tree we want to merge
 * this function merges two binary trees
*/
template<class T>
BST<T> mergeBST(BST<T> bst1,BST<T> bst2){
    vector<T> tree1,tree2,result;
    getBTElements(bst1,tree1);
    getBTElements(bst2,tree1);
    sort(tree1.begin(),tree1.end());
    BST<T> bst = makeBST(tree1);
    return bst;
}

void preOrderProcess(BSTNode<char>* t)
{
	if(t == nullptr)
		return;

	cout << t->elm << " " << endl;
	preOrderProcess(t->left);
	preOrderProcess(t->right);
}

/*
 * @param treeStack, the stack with BST type
 * string infix
 * this function constructs a new Expression tree with the data from infix
*/
BST<char> infix_ExprTree(string infix){
    BST<char> exp;
    Stack<BSTNode<char>*> treeStack;
    infix = Infix_PostfixExpr(infix);
    char operand1 = ' ';
    char operand2 = ' ';
    for(int counter = 0; counter < infix.length(); counter++ ){   
        if(infix[counter] != '+' && infix[counter] != '-' && infix[counter] != '*' && infix[counter] != '/'){
            treeStack.push(new BSTNode<char>{infix[counter]});
        }
        else{    
            BSTNode<char>* temp = new BSTNode<char>{infix[counter]};
            temp->right = treeStack.getTop();
            treeStack.pop();
            temp->left = treeStack.getTop();
            treeStack.pop();
            treeStack.push(temp);
        }
    }

    exp.root = treeStack.getTop();
    treeStack.pop();
    return exp;
}

string getExpression(){
    string input;
    getline(cin,input);
    return input;
}

int main(){
 // declaration of your variables ...
int n1 = getInput(); // either generates a random non-negative
 // integer or reads it from input
vector<int> list1 = genData(n1); //generates a list of n1 random numbers [-n1, n1]
cout << "The List1: ";
printList(list1); //prints elements of the given list
int n2 = getInput();
vector<int> list2 = genData(n2); //generates a list of n2 random numbers [-n2, n2]
cout << "The List2: ";
printList(list2);
BST<int> bst1 = makeBST(list1);
cout << "In-order traversal of bst1 is: ";
printBT(bst1);
remove(list1[n1/2], bst1); // removing the middle element of the corresponding
 // tree
cout << "In-order traversal of bst1 after deleting " << list1[n1/2] <<" is : ";
printBT(bst1);
BST<int> bst2 = makeBST(list2);
cout << "In-order traversal of bst2 is: ";
printBT(bst2);
BST<int> bst3 = mergeBST(bst1, bst2);
cout << "In_order traversal of bst3 is: ";
printBT(bst3);
cout << "The height of bst1 is " << height(bst1) << endl;
cout << "The height of bst2 is " << height(bst2) << endl;
cout << "The height of merged tree is " << height(bst3) << endl;
string infix = "(A*B)*C-D/E*F";//getExpression(); // read infix expression from input;
cout << "The Post-fix expression is " << Infix_PostfixExpr(infix) << endl;
BST<char> bt4 = infix_ExprTree(infix);
cout << "In-order traversal of bt4 is: ";
printBT(bt4);

clear(bst1.root);
clear(bst2.root);
clear(bst3.root);
clear(bt4.root);
}