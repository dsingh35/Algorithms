/*
 * @filename - Stack.cpp
 * @author - Devinder Singh
 * @date - 12/06/2018
*/
#include <vector>
#include "cstring"
#include<iostream>
using namespace std;


template <class T>
class Stack{

private:
	vector<T> list;
	int top = -1;
public:
	bool isEmpty(); 
	void push(T item); 
	T getTop(); 
	void pop(); 

};

template <class T>
bool Stack<T>::isEmpty() 
{
	return list.empty();
}

template <class T>
void Stack<T>::push(T item) 
{
	list.push_back(item);
	top++;
}

template <class T>
T Stack<T>::getTop() 
{
	if(top > -1)
		return list.back();
	else
		cerr<<"Stack is empty\n";
}

template <class T>
void Stack<T>::pop() 
{
	list.pop_back();
	top--;
}
