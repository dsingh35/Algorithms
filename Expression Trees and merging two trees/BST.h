/*
    @filename - BST.h
    @author - Devinder Singh
    @date - 2018/07/01 
*/
#ifndef BST_H
#define BST_H
#include <iostream>
using namespace std;


template<class T>
class BSTNode {
    public:
        BSTNode() { left = right = nullptr; }
        BSTNode( const T& e, BSTNode<T> *l = nullptr, BSTNode<T> *r = nullptr ) { 
            elm = e;
            left = nullptr;
            right = nullptr;
        }
        ~BSTNode() { delete left; delete right; }
        T elm;
        BSTNode<T> *left, *right;
};

template<class T>
class BST {
    public:
        BST() { root = new BSTNode<T>; };
        BST( BSTNode<T> *r ) { root = r; };
        BST( const T& e, BSTNode<T> *l = nullptr, BSTNode<T> *r = nullptr ) { root = new BSTNode<T>(e,l,r); };
        T printEl() { return root->elm; };
        BSTNode<T>* root;
};


#endif