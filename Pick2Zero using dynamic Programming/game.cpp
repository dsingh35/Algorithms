/*
 * @author Devinder Singh, Salvotore De Simone
 * @Date 2018/05/27
 * @fileName Game.cpp
 * @breif - 
 * this file has pick 2 zero game implementation 
*/

#include <iostream>
#include<ctime>
using namespace std;

int* array;// it is the aray in which win or loss scenarios will be stored
 
/*
 * @param array, the array that will be filled with -
 * @param n, the size of the array
 * this method fills the array with -1
*/
void fillArray(int* &array,int n) {
	for(int i = 0; i < n; i++){
		array[i] = -1;
	}
}

/*
 * @param n, the number we want to check where it leads us(win or loss)
 * this method calls itself and check every possible scenario by substracting every possible legit number
 * @return bool, true if the number is in winning scenario, false otherwise
*/
bool pick2Zero(int n){
	if(n == 0){
		return false;
	}
	if(n < 0){
		return true;
	}
	return array[n] = !pick2Zero(n-4) || !pick2Zero(n-3) || !pick2Zero(n-1);
}

/*
 * @param n, the number we want to check where it leads us(win or loss)
 * this method calls itself and check every possible scenario by substracting every possible legit number
 * but this is very efficient because doesn't evaluate the same number more than once
 * @return bool, true if the number is in winning scenario, false otherwise 
*/
bool pick2Zero_HC_efficient(int n){
	if(n == 0){
		return false;
	}
	if(n < 0){
		return true;
	}
	if(array[n] != -1){
		return array[n];
	}
	return array[n] = !pick2Zero_HC_efficient(n-4) || !pick2Zero_HC_efficient(n-3) || !pick2Zero_HC_efficient(n-1);
}

/*
 * @param n, the number where we want to find out the chances of win
 * this method calls pick2Zero_HC_efficient and checks for win or loss by substracting every allowed number
 *  @return counter, the number of chances one can win
*/
int countWinnings(int n){
	int counter = 0;;
	if(pick2Zero_HC_efficient(n-4) == false)
		counter++;
	if(pick2Zero_HC_efficient(n-3) == false)
			counter++;
    if(pick2Zero_HC_efficient(n-1) == false)
		counter++;

	return counter;		
}

/*
 * @param n, the number we want to check where it leads us(win or loss)
 * this method calls pick2Zero_HC_efficient and check every possible scenario by substracting every possible legit number
 * but this is very efficient because doesn't evaluate the same number more than once
 * @return int, the best possible option(number) to substract from n
*/
int pick2Zero_CC_efficient(int n){
	if(pick2Zero_HC_efficient(n-4) == false)
			n-=4;
	else if(pick2Zero_HC_efficient(n-3) == false)
			n-=3;
	else
			n-=1;
}

int main(){
	srand(time(0));
	int n = 1+rand()%6000;
	int vEric = 0, vComp = 0;
	array = new int(n);	
	fillArray(array,n);
	while(true){
		cout << endl << "The number is " << n << endl;
//---------------------------- comment this section for computer vs computer	

		cout << " the number of possibilities you can win are: " << countWinnings(n) << endl;
		cout << "Please enter 1,3 or 4" << endl;
		cin >> vEric;
		if((n-vEric) < 0)
		{
			cout << endl << "Error: You can't substract " << vEric << " from " << n << endl  << endl;
			continue;
		}
		while(vEric != 1 && vEric != 4 && vEric != 3){
			cout << "Error: Please enter 1,3 or 4" << endl;		
			cin >> vEric;
		}	
			n-=vEric;
//-----------------------------

		 //after commenting above section, please un comment the line below for Computer Vs Computer 
			
			//n-= pick2Zero_CC_efficient(n);
		
		
		if(n == 0){
			cout << "Congratulations Eric, you won!";
			delete[] array;
			exit(0);
		}	

		// checking by substracting the bigger number 
		if(pick2Zero_HC_efficient(n-4) == false)
			n-=4;
		else if(pick2Zero_HC_efficient(n-3) == false)
			n-=3;
		else
			n-=1;
	
		if(n==0){
			cout << "Sorry Eric, you lost!";
			delete[] array;			
			exit(0);
		}
	}
	return 0;
}