#include <string>
#include <vector>
#include <cmath>
#include <iostream>
#include <random>
#include <ctime>
#include <climits>
#define SIZE 1999
#define LOAD_FACTOR 0.5

int AVGINL = 0;
int AVGNOTINL = 0;
int AVGINQ = 0;
int AVGNOTINQ = 0;

using namespace std;

int unsigned getKey() {
   
    // /* Seed */
    // random_device rd;
    // /* Random number generator */
    // default_random_engine generator(rd());
    // /* Distribution on which to apply the generator */
    // uniform_int_distribution<long unsigned> distribution(0,0x7FFFFFFF);
    
    // return distribution(generator);
    return (1 + rand() % INT_MAX); 
}

long getLong(){
    return INT_MAX + (rand() % LONG_MAX);
}

int h(long unsigned x) {
    return (x % SIZE);
}

int searchL(vector<int> &T, int x,int key) {
    if (T[x] == -1) {
        return x;
    } 
    else { // UPPER PART OF TABLE CHECKING
        int startingPoint = x;
        int count = 0;
        while (x < SIZE && T[x] != -1) {
            if(T[x] == key){
                return -1;
            }
            x++;
        }
        if ( x != SIZE ) {
            return x;
        }
        else { // UPPER PART OF TABLE FULL - START FROM 0
            while (count < startingPoint && T[x] != -1) {                
                if(T[x] == key){
                    return -1;
                }
                count++;
            }
            if ( count != startingPoint ) {
                return count;
            } 
        }
    }
    return -1;
}

int insertL(vector<int> &T, int key) {
    int x = h(key);
    x = searchL(T,x,key);
    if(x == -1){
       // cout << "can't insert key " << key << ", table is full" << endl;
        return -1;
    }
    T[x] = key;
    return 0;
} 

int searchQ(vector<int> &T, int x,int key) {
    if (T[x] == -1){
        return x;
    } 
    int i = 1;
    int q = -1;
    while (i < SIZE && T[x] != -1) {
        if(T[x] == key)
            return -1;
        q = pow(q,i-1);
        x = (x+q) % SIZE;
        i++;
    }
    if(i < SIZE-1){
        return i;
    }
    return -1;
}

int insertQ(vector<int> &T, int key) {
    int x = h(key);
    int index = searchQ(T,x,key);
    if(x == -1){
       // cout << "can't insert key " << key << ", table is full" << endl;     
        return -1;
    }
    T[index] = key;
    return 0;
}

void fillTable(vector<int> &T, int whichProbing,long &keys) {
    T.assign(SIZE,-1);
    int i = 0;
    while (i < 1000) {
        long unsigned key = getKey();
        if ( whichProbing == 1) {
            if(insertL(T,key) != -1){
                    keys = key;
                i++;
            }
        } else {
            if(insertL(T,key) != -1){ 
                keys = key;
                i++;
            }
        }
    } 
}

void printTable(vector<int> &T) {
    for (auto x: T) {
        cout << x << endl;
    }
}

int searchKeyL(vector<int> &T,int key){
    int x = h(key);
    int counter = 0;
    while(counter < SIZE){
        if(T[x%SIZE] == key){
           // cout << key << " is in the table at index " << x%SIZE << endl; 
            return counter+1;
        }
        if(T[x%SIZE] == -1){
            //cout << key << " is not in the table" << endl; 
            return counter+1;
        }
        counter++;x++;
    }
    return counter;
}


int searchKeyQ(vector<int> &T,int key){
    int x = h(key);
    int i = 1;
    int q = -1;
    
    while (i < SIZE) {
        if(T[x] == key){
           // cout << key << " is in the table at index " << x%SIZE << endl; 
            return i;
        }
        if(T[x] == -1){
            //cout << key << " is not in the table" << endl; 
            return i;
        }
        q = pow(q,i-1);
        x = (x+q) % SIZE;
        i++;
    }
    return i;
}

int main() {
    srand(time(NULL));
    int repeat = 100;

    for(int i = 0; i < repeat; i++){
        vector<int> hashTableL;
        vector<int> hashTableQ;
        long keysToSearch;

        fillTable(hashTableL,1,keysToSearch);

    // ------------------ when keys are present -------------------------    
        for(int i = 0; i < repeat; i++){
            AVGINL += searchKeyL(hashTableL,keysToSearch); 
        }


        fillTable(hashTableQ,2,keysToSearch);
        
        for(int i = 0; i < repeat; i++){
            AVGINQ += searchKeyL(hashTableL,keysToSearch); 
        }

    //------------------------------------------

    //--------------- when keys are not present ----------------------------
        
        for(int i = 0; i < repeat; i++){
            keysToSearch = getLong();
        }

        for(int i = 0; i < repeat; i++){
            AVGNOTINL += searchKeyL(hashTableL,keysToSearch); 
        }

        
        for(int i = 0; i < repeat; i++){
            AVGNOTINQ += searchKeyL(hashTableL,keysToSearch); 
        }

    //---------------------------------------------------
    }

    cout << endl << "when keys are present:-  " << endl;
    
    cout << "average for searching a key in linear probing is " << (AVGINL/100)  << endl;  
    cout << "average for searching a key in quadratic probing is " << (AVGINQ/100) << endl;  

    cout << endl << "when keys are not present:-  " << endl;

    cout << "average for searching a key in linear probing when key is not present is " << (AVGNOTINL/100)  << endl;  
    cout << "average for searching a key in quadratic probing when key is not present is " << (AVGNOTINQ/100)  << endl;  

    return 0;
}