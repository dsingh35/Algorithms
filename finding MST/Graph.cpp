#include <iostream>
#include <vector>
#include <climits>
#include <fstream>
#include <bits/stdc++.h>
using namespace std;
 
 
struct Edge{
    int source;
    int destination;
    int label;
};

void copyToConnected(vector<pair<int,int> > adj[], int V,vector<Edge*>& edges){
    for (int u = 0; u < V; u++){
        for (auto it = adj[u].begin(); it!=adj[u].end(); it++){        
            edges.push_back(new Edge{u,it->first,it->second});
        }
    }    
}

void printAllEdges(vector<Edge*> & edges){
    int counter = 0;
    for(Edge* e : edges){
        counter++;
        cout << e->source << "  --" << e->label << "--> " << e->destination << endl; 
    }
    cout << "counter is " << counter << endl;
}
// To add an edge
void addEdge(vector <pair<int,int>> adj[], int u,int v, int wt)
{
    adj[u].push_back(make_pair(v, wt));
    adj[v].push_back(make_pair(u, wt));
}
 
// Print adjacency list representaion ot graph
void printGraph(vector<pair<int,int> > adj[], int V)
{
    int v, w;
    for (int u = 0; u < V; u++)
    {
        cout << "Node " << u << " makes an edge with \n";
        for (auto it = adj[u].begin(); it!=adj[u].end(); it++)
        {
            v = it->first;
            w = it->second;
            cout << "\tNode " << v << " with edge weight = "
                 << w << "\n";
        }
        cout << "\n";
    }
}
 
bool compareEdges(const Edge * f, const Edge * s){
     return f->label < s->label;
}

void findMST(vector<Edge*> &edges){
    vector<Edge*> mst;
    int counter = 0;
    mst.push_back(edges[0]);
    cout << edges.size() << endl;
    cout << "asbhabdhjabs" << mst.size() << " ";

    for(int i = 1; i < edges.size(); i++){
            cout << i << endl;
        for(int j = 0; j < mst.size(); j++){
            if((edges[i]->source == mst[j]->source && edges[i]->destination == mst[j]->destination) ||
                (edges[i]->destination == mst[j]->source && edges[i]->source == mst[j]->destination)){
                //delete e;
                //e = nullptr;
            }
            else{
                mst.push_back(edges[i]);
                break;
            }
        }
    }

    printAllEdges(mst);
}

// Driver code
int main()
{
    vector<Edge*> connectedEdges;
    ifstream inFile;
    inFile.open("graph25.txt");
    if (!inFile) {
    cerr << "Unable to open file datafile.txt";
    exit(1);   // call system to stop
    }
    int V;
    inFile >> V;
    vector<pair<int, int> > adj[V];
    while ( !inFile.eof() ) {
        int f,s,w;
        inFile >> f;
        inFile >> s;
        inFile >> w;
        addEdge(adj,f,s,w);
    }
    inFile.close();

    //printGraph(adj, V);
   // for (int u = 0; u < V; u++)
     //   sort(adj[u].begin(), adj[u].end());
    cout << "------------------------sorted version -----------------------------" << endl;
    copyToConnected(adj,V,connectedEdges);
    sort(connectedEdges.begin(),connectedEdges.end(),compareEdges);
    //printAllEdges(connectedEdges);
    findMST(connectedEdges);
    return 0;
}