/*
    @filename - NaiveSort.cpp
    @author - Devinder Singh
    @date - 2018/07/01 
*/
#include <cmath>
#include <climits>
#include <ctime>
#include <iostream>
#include <cstdlib>
using namespace std;

/*
 * @return int, the input by user
 * this function takes input from the user 
*/
int getInput() { 
    int n;
    cout << "Enter a positive integer: ";
    cin >> n;
    return n;
}

/*
 * @param a, the vector containing integers
 * this function prints every element of vector
*/
void printList(int * list, int size) { 
    for (int i = 0; i < size; i++) 
        cout << list[i] << " "; 
    cout << endl;
}

/*
    * @param list , the we want to initilize
    * @param size, the size of the list
*/
void initList(int * list, int size,int value=0){
    for(int i = 0; i < size; i++){
        list[i] = value;
    }
}

/*
    * @param data , the array we want to fill
    * @param size, the size of the list
*/
void fillTheLeaves(int *tree, int * data, int size,int height){
    for(int i = 0; i < size; i++){
        int index = pow(2,height-1)-1+i;
        tree[index] = data[i];
    }
}

void changeTheOriginalList(int * data, int index,int value){
    data[index] = value;
}

void replaceTheMinimun(int *tree,int value,int size){
    for(int i = 0; i < size; i++){
        if(tree[i] == value)
            tree[i] = INT_MAX;
    }
}

/*
    * @param tree , the array we want to sort
    * @param height, the height of the tree
*/
void fillInternalNodes(int *tree, int height){
    for(int h = height; h > 1; h--){
        for(int i = 0; i < pow(2,h-1); i+=2){
            int parent = pow(2,h-2)-1;        
            int left = pow(2,h-1)-1+i;
            int right = pow(2,h-1)+i;
            
            if(tree[left] < tree[right])
            {
                tree[parent+i/2] = tree[left];    
            }
            else{
                tree[parent+i/2] = tree[right];                
            }
        }  
    }      
}

/*
    * @param tree , the array we want to sort
    * @param height, the height of the tree
    * @param sizeOfData, the size of the data
*/
void naiveSort(int* data, int *tree, int sizeOfData,int sizeOfTree,int height){
    fillTheLeaves(tree,data,sizeOfData,height);
    int counter = 0;
    while(counter < sizeOfData){
        fillInternalNodes(tree,height);
        changeTheOriginalList(data,counter,tree[0]);
        replaceTheMinimun(tree,tree[0],sizeOfTree);
        counter++;
    }
}

int main(){
    int n = 5;
    int h = ceil(log2(n)) + 1;
    int emptyTreeSize = pow(2,h)-1;
    int* data = new int(n); 
    initList(data,n,8);
    data[1] = 20;
    data[2] = 0;
    data[3] = 7;
    data[4] = 2;
    

    int* emptyList = new int(emptyTreeSize);

    initList(emptyList,emptyTreeSize,INT_MAX);
    cout << "list before Sorting:" << endl;
    printList(data,n);
    naiveSort(data,emptyList,n,emptyTreeSize,h);    
    cout << "list after Sorting:" << endl;    
    printList(data,n);

    delete[] emptyList,data;
    return 0;
}