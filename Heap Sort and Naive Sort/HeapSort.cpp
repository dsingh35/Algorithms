/*
    @filename - HeapSort.cpp
    @author - Devinder Singh
    @date - 2018/07/01 
*/
#include <cmath>
#include <ctime>
#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

/*
 * @param a, list containing integers
 * this function prints every element of list
*/
void printList(int * list, int size) { 
    for (int i = 0; i < size; i++) 
        cout << list[i] << " "; 
    cout << endl;
}

/*
 * @param array, the the array we want to sort
 * @param size, the size of the list
 * @param index, the index of the max
*/
void heapify(int* array,int size, int index){
    
    int max = index;
    int left = 2*index + 1;
    int right = 2*index + 2;

    if(right < size  && array[right] > array[max])
        max = right;
    
    if(left < size  && array[left] > array[max])
        max = left;
            
    if(max != index){
        swap(array[index],array[max]);
        heapify(array,size,max);
    }    
}

void fill(int* data ,int size){
    for(int i= 0; i < size; i++){
        data[i] = 0+rand()%100;
    }
}
/*
 * @param data, the the array we want to sort
 * @param size, the size of the list
*/
void heapSort(int*data,int size){
    
   for(int i = size/2-1;i >= 0; i--){ // for arranging the array 
       heapify(data,size,i);
   } 

    printList(data,size);
    
    for (int i = size-1; i >= 0; i--)
    {
        swap(data[0], data[i]);
        heapify(data, i, 0);
    }    
}

int main(){
    int n = 53;
    int* data = new int(n); 
    fill(data,n);
    cout << "list before Sorting: " << endl;
    printList(data,n);
    heapSort(data,n);
    cout << "list after Sorting: " << endl;
    printList(data,n);
    delete[] data;
    return 0;
}