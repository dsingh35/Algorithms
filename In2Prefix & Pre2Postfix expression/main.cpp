/*
 * @filename - TestExpressions.cpp
 * @author - Devinder Singh
 * @date - 12/06/2018
*/
#include "Stack.h"
#include <cstring>
#include <iostream>
using namespace std;

char openRound = '(';
char closedRound = ')';
char openCurly = '{';
char closedCurly = '}';

/*
 * @param bracket, the bracket whose opposite we want to find
 * return char, the opposite bracket
*/
char getRequiredBraket(char bracket){
    switch(bracket){
        case '{':
        return '}';
        case '(':
        return ')';
        case '[':
        return ']';
    }
}

/*
 * @param s, the string we want to reverse
*/
void reverseString(string & s){
    string c = s;
    for(int i = c.length()-1,j=0; i >= 0; i--,j++){
        s[j] = c[i];
    }
}

/* @param stack, the object of class Stack
 * @param infix , the string we want to change
 * @counter , to iterate through string
 * this function changes infix to prefix
*/
string in2Prefix(Stack<char> & stack,string &infix,int counter=0){
    string result = "";
     reverseString(infix);
     infix = ')' + infix + '('; // adding brackets to escape specail cases
   
        for( counter = 0; counter < infix.length(); counter++ ){
        //if the input is alphabet or digit
        if(infix[counter] != ')' && infix[counter] != ']'&& infix[counter] != '['&& infix[counter] != '}' && infix[counter] != '{' && infix[counter]!= '(' && infix[counter] != '+' && infix[counter] != '-' && infix[counter] != '*' && infix[counter] != '/')
        {
            result+= infix[counter];
        }
        else if(infix[counter] == ')' || infix[counter] == '}' || infix[counter] == ']' )
            stack.push(infix[counter]);
        else if(infix[counter] == '[' || infix[counter] == '(' || infix[counter] == '{'){
            char requiredBRacket = getRequiredBraket(infix[counter]);// getting the opposite bracket
            while(!stack.isEmpty() && stack.getTop() != requiredBRacket){
                if(stack.getTop() == '{' || stack.getTop() == '(' || stack.getTop() == '[' || stack.getTop() == '}' || stack.getTop() == ')' || stack.getTop() == ']')
                    {cout <<  "Error: '" << requiredBRacket << "' was expected at" <<  " '" << stack.getTop() << "'" << endl;
                    return "";
                 }
                char pop = stack.getTop();
                result+= pop;
                stack.pop();
            }
            if(stack.getTop() == ')')
                stack.pop();
        }
        else{ // if the input is operator
                while(!stack.isEmpty())
                {   
                    if((infix[counter] == '/' || infix[counter] == '*') &&( stack.getTop() == '+' || stack.getTop() == '-')){
                        char pop = stack.getTop();
                        result+= pop;
                        stack.pop();
                    }
                    else
                        break;                 
                }    
            
            stack.push(infix[counter]); 
        }
    }
  
    while(!stack.isEmpty())
    {
        char c = stack.getTop();
        stack.pop();
        result += c;
    }

    reverseString(result);
    return result; 
}

/* @param stack, the object of class Stack
 * @param prefix , the string we want to change
 * @counter , to iterate through string
 * this function changes prefix to postfix
*/
string pre2Post(Stack<char> &stack,string & prefix ,int counter){
    static string result = "";

    if(counter < 0){
        while(!stack.isEmpty()){
            result = stack.getTop() + result;
            stack.pop();
        }
        return result;
    }

    // escaping spaces and brackets
    if(prefix[counter] == ' ' || prefix[counter] == '(' ||prefix[counter] == ')' ||prefix[counter] == '{' ||prefix[counter] == '}' )
        return pre2Post(stack,prefix,counter-1);

    // if input is operaotr
    if( prefix[counter] == '+' || prefix[counter] == '-' || prefix[counter] == '*' || prefix[counter] == '/'){
        if(!stack.isEmpty()){
            string temp = "";
            while(!stack.isEmpty()){
                temp+= stack.getTop();
                stack.pop();
            }
            temp+= prefix[counter];
            result = temp + result;
        }
        else
            result+= prefix[counter]; 
    }
    else{
        stack.push(prefix[counter]);
    }

    return pre2Post(stack,prefix,counter-1);
}

int main(){
    Stack<char> stack;
    string infix = "(a*b)/(c*d)";
    cout << "Prefix of infix expression: " << infix << endl;

    string result = in2Prefix(stack,infix,0);
    cout << result << endl;

    string prefix = "/*ab*cd";    
    cout << "Postfix of prefix expression: " << prefix << endl;
    result = pre2Post(stack,prefix,prefix.length()-1);
    cout << result << endl;
    
    //------------

    infix = "(a/b-c+d)*(e-a)*c";
    cout << "Prefix of infix expression: " << infix << endl;
    result = in2Prefix(stack,infix,0);
    cout << result << endl;
    cout << "Postfix of prefix expression: " << result << endl;
    result = pre2Post(stack,result,result.length()-1);
    cout << result << endl;

    //------------

    infix = "(a*b)/{c*d)";
    cout << "Prefix of infix expression: " << infix << endl;
    result = in2Prefix(stack,infix,0);
    cout << result << endl;
    return 0;
}
